package json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ReadmealJSONFile {
//        static ArrayList<String> n = new ArrayList<String>();
//        static ArrayList<String> n1 = new ArrayList<String>();
//        static ArrayList<Long> n2 = new ArrayList<Long>();
//        static ArrayList<Long> n3 = new ArrayList<Long>();
        
    public static void main(String[] args) {
//        System.out.println(Read(1));
//        System.out.println(readMeal(1));
        System.out.println(readMealsStatisticsName());
        System.out.println(readMealsStatisticsAmount());
        System.out.println(readMealsStatisticsCost());
        ArrayList<Long> mealEarn = new ArrayList<Long>();
		for (int i = 0; i < readMealsStatisticsName().size(); i++) {
			mealEarn.add(readMealsStatisticsAmount().get(i) * readMealsStatisticsCost().get(i));
		}
		System.out.println(mealEarn);

    }
   
    public static JSONArray Read(int table)
    {
        JSONParser parser = new JSONParser();
        try
        {
            Object obj = parser.parse(new FileReader("meal"+table+".json"));
            JSONArray jsonObject = (JSONArray) obj; 
            
            return jsonObject;
        }

        catch(FileNotFoundException e){e.printStackTrace();}
        catch(IOException e){e.printStackTrace();}
        catch(ParseException e){e.printStackTrace();}
        catch(Exception e){e.printStackTrace();}
        return null;
    }
    
    public static ArrayList<String> readMeal(int table)
    {
        JSONParser parser = new JSONParser();
        ArrayList<String> n = new ArrayList<String>();
        n.clear();
        try
        {
            Object obj = parser.parse(new FileReader("meal"+table+".json"));
            JSONArray jsonObject = (JSONArray) obj; 
            
            for(int i = 0;i<jsonObject.size();i++){
                JSONObject a =(JSONObject) jsonObject.get(i);
                n.add((String)a.get("name")) ;
            }

            return n;
        }

        catch(FileNotFoundException e){e.printStackTrace();}
        catch(IOException e){e.printStackTrace();}
        catch(ParseException e){e.printStackTrace();}
        catch(Exception e){e.printStackTrace();}
        return n;
    }
    
    public static ArrayList<Long> readMealCost(int table)
    {
        JSONParser parser = new JSONParser();
        ArrayList<Long> n3 = new ArrayList<Long>();
        n3.clear();
        try
        {
            Object obj = parser.parse(new FileReader("meal"+table+".json"));
            JSONArray jsonObject = (JSONArray) obj; 
            
            for(int i = 0;i<jsonObject.size();i++){
                JSONObject a =(JSONObject) jsonObject.get(i);
                n3.add((Long)a.get("cost")) ;
            }

            return n3;
        }

        catch(FileNotFoundException e){e.printStackTrace();}
        catch(IOException e){e.printStackTrace();}
        catch(ParseException e){e.printStackTrace();}
        catch(Exception e){e.printStackTrace();}
        return n3;
    }
    
    public static ArrayList<String> readMealsStatisticsName(){
    	ArrayList<String> n1 = new ArrayList<String>();
        n1.clear();
        ArrayList<String> temAL = readMeal(1);
        for (int i = 1; i < temAL.size(); i++) {
        	n1.add(temAL.get(i));
        }
        temAL = readMeal(2);
        for (int i = 1; i < temAL.size(); i++) {
        	n1.add(temAL.get(i));
        }
        temAL = readMeal(3);
        for (int i = 1; i < temAL.size(); i++) {
        	n1.add(temAL.get(i));
        }
        temAL = readMeal(4);
        for (int i = 1; i < temAL.size(); i++) {
        	n1.add(temAL.get(i));
        }
        return n1;
    }
    
    public static ArrayList<Long> readMealsStatisticsCost(){
    	ArrayList<Long> n4 = new ArrayList<Long>();
    	n4.clear();
        ArrayList<Long> temAL = readMealCost(1);
        for (int i = 1; i < temAL.size(); i++) {
        	n4.add(temAL.get(i));
        }
        temAL = readMealCost(2);
        for (int i = 1; i < temAL.size(); i++) {
        	n4.add(temAL.get(i));
        }
        temAL = readMealCost(3);
        for (int i = 1; i < temAL.size(); i++) {
        	n4.add(temAL.get(i));
        }
        temAL = readMealCost(4);
        for (int i = 1; i < temAL.size(); i++) {
        	n4.add(temAL.get(i));
        }
        return n4;
    }
    
    public static ArrayList<Long> readMealsStatisticsAmount(){
    	JSONParser parser = new JSONParser();
    	ArrayList<Long> n2 = new ArrayList<Long>();
        n2.clear();
        ArrayList<String> temAL = readMealsStatisticsName();
        try
        {
            Object obj = parser.parse(new FileReader("mealsstatistics.json"));
            JSONObject jsonObject = (JSONObject) obj; 
            for (String mealName : temAL) {
            	n2.add((Long) jsonObject.get(mealName)) ;
            }

            return n2;
        }

        catch(FileNotFoundException e){e.printStackTrace();}
        catch(IOException e){e.printStackTrace();}
        catch(ParseException e){e.printStackTrace();}
        catch(Exception e){e.printStackTrace();}
        return n2;
    }
    
}