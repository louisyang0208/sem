package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SortOrder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import json.*;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Manager_DataAnalysis extends JFrame {
	
	private DefaultPieDataset<String> pieDataset;
	private JFreeChart pieChart;
	private PiePlot piePlot;
	private ChartPanel chartPanel;
	private JPanel pieChartPanel;
	ArrayList<String> mealNameAL;
	ArrayList<Long> mealAmountAL;
	ArrayList<Long> mealCostAL;
	private DefaultPieDataset<String> pieDataset2;
	private JFreeChart pieChart2;
	private PiePlot piePlot2;
	private ChartPanel chartPanel2;
	private JPanel pieChartPanel2;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Manager_DataAnalysis frame = new Manager_DataAnalysis();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Manager_DataAnalysis() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1280, 720);
		getContentPane().setLayout(null);

		pieChartPanel = new JPanel();
		pieChartPanel.setBounds(30, 35, 450, 300);
		getContentPane().add(pieChartPanel);
		pieChartPanel.setLayout(new BorderLayout(0, 0));
		showPieChart();
		
		pieChartPanel2 = new JPanel();
		pieChartPanel2.setBounds(520, 35, 450, 300);
		getContentPane().add(pieChartPanel2);
		pieChartPanel2.setLayout(new BorderLayout(0, 0));
		showPieChart2();
		
		JLabel lblNewLabel = new JLabel("人員效率(服務生)");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		lblNewLabel.setBounds(40, 400, 149, 35);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("平均周轉時間");
		lblNewLabel_1.setFont(new Font("平均周轉時間", Font.BOLD, 18));
		lblNewLabel_1.setBounds(580, 537, 149, 35);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("平均準備時間");
		lblNewLabel_2.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		lblNewLabel_2.setBounds(580, 400, 149, 35);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("人員效率(廚師)");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		lblNewLabel_3.setBounds(40, 537, 149, 35);
		getContentPane().add(lblNewLabel_3);
		
		textField = new JTextField();
		textField.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		textField.setEditable(false);
		textField.setBounds(705, 535, 156, 35);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		textField_1.setEditable(false);
		textField_1.setBounds(705, 400, 156, 35);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		textField_2.setEditable(false);
		textField_2.setBounds(199, 400, 156, 35);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("微軟正黑體", Font.BOLD, 18));
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(199, 537, 156, 35);
		getContentPane().add(textField_3);
		
		JButton btnNewButton = new JButton("刷新");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPieChart();
				showPieChart2();
				setTextField();
			}
		});
		btnNewButton.setFont(new Font("微軟正黑體", Font.BOLD, 14));
		btnNewButton.setBounds(1140, 35, 97, 35);
		getContentPane().add(btnNewButton);
		
		setTextField();
	}
	
	public void showPieChart() {
		pieDataset = new DefaultPieDataset<String>();
		this.mealNameAL = ReadmealJSONFile.readMealsStatisticsName();
		this.mealAmountAL = ReadmealJSONFile.readMealsStatisticsAmount();
		bubbleSort(mealAmountAL, mealNameAL);


		for (int i = 0; i < mealNameAL.size(); i++) {
			pieDataset.setValue(mealNameAL.get(i), mealAmountAL.get(i));
		}

		pieChart = ChartFactory.createPieChart3D("菜單項目受歡迎程度", pieDataset, false, false, false);
		
		TextTitle textTitle = pieChart.getTitle();
		textTitle.setFont(new Font("宋體",Font.BOLD,15));
		
		LegendTitle legend = pieChart.getLegend();   
		if (legend!=null) {   
			legend.setItemFont(new Font("宋體", Font.PLAIN, 15));   
		}
		
		piePlot = (PiePlot) pieChart.getPlot();
		piePlot.setLabelFont(new Font("宋體",Font.PLAIN,12));
		piePlot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({2})"));
		chartPanel = new ChartPanel(pieChart);
		chartPanel.setForeground(new Color(255, 255, 255));
		chartPanel.setBackground(new Color(255, 128, 128));
		pieChartPanel.removeAll();
		pieChartPanel.add(chartPanel, BorderLayout.CENTER);
		chartPanel.validate();
		
		
	}
	
	public void showPieChart2() {
		pieDataset2 = new DefaultPieDataset<String>();
		this.mealNameAL = ReadmealJSONFile.readMealsStatisticsName();
		this.mealAmountAL = ReadmealJSONFile.readMealsStatisticsAmount();
		this.mealCostAL = ReadmealJSONFile.readMealsStatisticsCost();
		ArrayList<Long> mealEarn = new ArrayList<Long>();
		for (int i = 0; i < mealNameAL.size(); i++) {
			mealEarn.add(mealAmountAL.get(i) * mealCostAL.get(i));
		}
		

		bubbleSort(mealEarn, mealNameAL);


		for (int i = 0; i < mealNameAL.size(); i++) {
			pieDataset2.setValue(mealNameAL.get(i), mealEarn.get(i));
		}

		pieChart2 = ChartFactory.createPieChart3D("菜單項目的收入和收入百分比", pieDataset2, false, false, false);
		
		TextTitle textTitle = pieChart2.getTitle();
		textTitle.setFont(new Font("宋體",Font.BOLD,15));
		
		LegendTitle legend = pieChart2.getLegend();   
		if (legend!=null) {   
			legend.setItemFont(new Font("宋體", Font.PLAIN, 15));   
		}
		
		piePlot2 = (PiePlot) pieChart2.getPlot();
		piePlot2.setLabelFont(new Font("宋體",Font.PLAIN,12));
		piePlot2.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({1},{2})"));
		chartPanel2 = new ChartPanel(pieChart2);
		chartPanel2.setForeground(new Color(255, 255, 255));
		chartPanel2.setBackground(new Color(255, 128, 128));
		pieChartPanel2.removeAll();
		pieChartPanel2.add(chartPanel2, BorderLayout.CENTER);
		chartPanel2.validate();
		
		
	}
	
	public void setTextField() {
		textField.setText((UpdatetimeJSONFile.Read("staytime.json").size()*1.0 / 2.0) + "訂單/人");
		textField_1.setText(UpdatetimeJSONFile.readAverage("usetime.json") + "秒");
		textField_2.setText((UpdatetimeJSONFile.Read("staytime.json").size()*1.0 / 2.0) + "桌次/人");
		textField_3.setText(UpdatetimeJSONFile.readAverage("staytime.json") + "秒");
	}

	public void bubbleSort(ArrayList<Long> AL1, ArrayList<String> AL2) {
        if (AL1.size() == 0)
            return;
        for (int i = 0; i < AL1.size(); i++)
            for (int j = 0; j < AL1.size() - 1 - i; j++)
                if (AL1.get(j + 1) < AL1.get(j)) {
                    Long temp = AL1.get(j + 1);
                    AL1.set(j + 1, AL1.get(j));
                    AL1.set(j, temp);
                    
                    String temp2 = AL2.get(j + 1);
                    AL2.set(j + 1, AL2.get(j));
                    AL2.set(j, temp2);
                }
        return;
    }
}
